function toggleParagraph(id, toggleBtn) {
  let paragraph = document.getElementById(id);
  if (paragraph.style.display === "none" || paragraph.style.display === "") { /* on affiche */
    paragraph.style.display = "block";
    toggleBtn.innerHTML = "Afficher moins";
  } else { /* on cache */
    paragraph.style.display = "none";
    toggleBtn.innerHTML = "Afficher plus";
  }
}

for (let i = 1; i < 5; i++) {
  const paragraph_div = document.getElementById("paragraph" + i + "_div");

  let toggleBtn = document.createElement("a");
  paragraph_div.insertBefore(toggleBtn, paragraph_div.firstChild);
  toggleBtn.innerHTML = "Afficher plus";
  toggleBtn.onclick = function () { toggleParagraph("paragraph" + i, toggleBtn); };
  toggleBtn.setAttribute("class", "paragraph_link");

  let title = document.createElement("span");
  title.innerHTML = "Paragraphe " + i;
  paragraph_div.insertBefore(title, paragraph_div.firstChild);
}

